#!/bin/bash
#Install kube-prometheus-stack

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ];
  then
    echo "Syntax: ./install.sh [env] [dc] [cluster_name]"
    exit 1
fi

#Vars
export ENV=$1
export DC=$2
export CLUSTER_NAME=$3
VERSION=48.4.0
VICTORIA_LB="http://victoria-balancer.srelab.net:8480/insert/0/prometheus/"
EXTERNAL_URL="http://prometheus.monitoring.$CLUSTER_NAME"
ALERT_EXTERNAL_URL="http://alertmanager.monitoring.$CLUSTER_NAME"
export KUBECONFIG=./config
export BOT_TOKEN=`vault kv get -field=BOT_TOKEN secrets/infrastructure/chat_tokens/telegram/$ENV`
export CHAT_ID=`vault kv get -field=CHAT_ID secrets/infrastructure/chat_tokens/telegram/$ENV`
export CHAT_ID_CRIT=`vault kv get -field=CHAT_ID_CRIT secrets/infrastructure/chat_tokens/telegram/$ENV`
export ROCKET_WEBHOOK=`vault kv get -field=WEBHOOK secrets/infrastructure/chat_tokens/rocket/$ENV`
export ROCKET_WEBHOOK_CRIT=`vault kv get -field=WEBHOOK_CRIT secrets/infrastructure/chat_tokens/rocket/$ENV`

# Generate values.yaml
helm show values prometheus-community/kube-prometheus-stack > values.yaml

# Create ns
kubectl create ns monitoring
# Configure prometheus operator values.yaml for helm
yq ".nodeExporter.enabled = false" -i values.yaml # disable node exporter
yq  ".prometheus.prometheusSpec.externalLabels.cluster_name = \"$CLUSTER_NAME\"" -i values.yaml #enable exteral lables for tsdb
yq  ".prometheus.prometheusSpec.remoteWrite.0.url = \"$VICTORIA_LB\"" -i values.yaml #set remote write victoria url
yq ".prometheus.prometheusSpec.externalUrl = \"$EXTERNAL_URL\"" -i values.yaml #set prometheus external url

# Helm install
##install repo
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

##install operator
helm upgrade --install monitoring prometheus-community/kube-prometheus-stack -f ./values.yaml -n monitoring --debug --version $VERSION #install helm chart

#Configuring Ingresses
envsubst < ./templates/prom-ingress.yaml > prom-ingress.yaml
kubectl apply -f prom-ingress.yaml #create ingress for prometheus
envsubst < ./templates/alertmanager-ingress.yaml > alertmanager-ingress.yaml
kubectl apply -f alertmanager-ingress.yaml #create ingress for alertmanager
envsubst < ./templates/grafana-ingress.yaml > grafana-ingress.yaml
kubectl apply -f grafana-ingress.yaml #create ingress for grafana
rm prom-ingress.yaml alertmanager-ingress.yaml grafana-ingress.yaml

echo "Sleep 15 sec for start prometheus operator"
sleep 15

# Additional Scrape Configuration - add secret additional-scrape-configs.yaml
envsubst < ./templates/prometheus-additional.yaml > prometheus-additional.yaml
kubectl create secret generic additional-scrape-configs --from-file=prometheus-additional.yaml --dry-run=client -oyaml > additional-scrape-configs-secret.yaml
kubectl apply -f additional-scrape-configs-secret.yaml -n monitoring
rm prometheus-additional.yaml additional-scrape-configs-secret.yaml

# Additional Scrape Configuration - configure crd prometheus.yaml for use additional-scrape-configs.yaml
kubectl get prometheus -n monitoring -o yaml > prometheus-crd.yaml # get crd prometheus operator config
yq '.items.0.spec.additionalScrapeConfigs.name = "additional-scrape-configs"' -i prometheus-crd.yaml
yq '.items.0.spec.additionalScrapeConfigs.key = "prometheus-additional.yaml"' -i prometheus-crd.yaml
kubectl apply -f prometheus-crd.yaml
rm prometheus-crd.yaml

#Configure Alertmanager
envsubst < ./templates/alertmanager.yaml > alertmanager.yaml # get token from exported var $TOKEN
kubectl create secret generic alertmanager-monitoring-kube-prometheus-alertmanager --from-file=alertmanager.yaml --dry-run=client -oyaml > alertmanager-config.yaml
kubectl apply -f alertmanager-config.yaml -n monitoring
# Set alertmanager external url in alert manager crd
kubectl get alertmanager -n monitoring -o yaml > alertmanager-crd.yaml
yq ".items.0.spec.externalUrl = \"$ALERT_EXTERNAL_URL\"" -i alertmanager-crd.yaml #set alertmanager external url
kubectl apply -f alertmanager-crd.yaml
rm alertmanager.yaml alertmanager-config.yaml values.yaml alertmanager-crd.yaml
echo "!!! Grafana default password: prom-operator"
